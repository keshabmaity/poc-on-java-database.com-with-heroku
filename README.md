# Spring MVC Template for Salesforce

This is a template for a Spring MVC web application to work with data from Database.com.
The sample code is a dynamic CRUD application that allows users to create, read, edit, and delete any record stored in custom object object.
It is backed by the [Rich SObjects](https://github.com/ryanbrainard/richsobjects) library for interactions with Salesforce.

## Getting Started

1. [__Clone Now__](https://bitbucket.org/keshab/poc-on-java-database.com-with-heroku/clone)
2. Setup your databse.com OAuth Remote Access. You will then have a OAuth client key and secret
3. Update the Environment variables to include the OAuth client key and secret
4. Navigate to the "Click to view All User  or 
Click to Create New User" page and you should now be authenticated against databse.com and view/create/update/delete Users
